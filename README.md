# Desciption du projet

## Auteurs

Ce prjet est réalisé par **Chris TRAMIER et Mike AUBENAS**.

## Contexte

Dans le cadre de nos cours de JavaScript en Licence 3 MIAGE à Nice Sophia-Antipolis nous devons réaliser un petit projet en binome afin de mettre en pratique les enseignements que nous avons assimilé dans la matière.

## Explication du jeu

*En tant qu'étudiant Miagiste et il est temps pour vous de réussir votre année scolaire.*

Pour réussir votre année vous allez devoir cliquer sur le plus de matières possibles, ainsi vous accumulerez des points qui vous seront essentiels pour obtenir votre license.

`Attention à vous !`
 
 Le temps vous est imparti mais vous pouvez l'ajuster avec le curseur en haut de votre écran !

>Amuser-vous bien !!
